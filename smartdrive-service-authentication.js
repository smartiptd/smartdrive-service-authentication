#!/usr/bin/env node

/* Module dependencies */
var express = require('express');
var mongoose = require('mongoose');
var flash = require('express-flash');
var morgan = require('morgan');
var http = require('http');
var https = require('https');
var fs = require('fs');

var config = require('config');
var logger = require('smartdrive-lib').logger;
var pkg = require('./package.json');

var passport = require('passport');

/* App */
var app = express();
var appConf = config.get('app');
app.set('port', appConf.config.sslport);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('secretKey'));
app.use(express.session({secret: 'secretKey'}));
app.use(express.static(__dirname + '/public'));
app.use(flash());

/* Configure passport middleware */
app.use(passport.initialize());
app.use(passport.session());

/* Connect to database */
var db = mongoose.connection;
db.on('error', function(err) {
    logger.error('Cannot connect to database', {error:err});
});

db.once('connected', function() {
    logger.info('Database connected');
});

db.on('disconnected', function() {
    logger.info('Database disconnected');
});

var dbconf = config.get('db');
mongoose.connect('mongodb://' + dbconf.host + '/' + dbconf.dbName);

/* Routing */
// require('./routes/auth')(passport);
// require('./api')(passport, app);
require('./routes')(passport, app);

/* Error handler */
app.use(function(err, req, res, next) {
    logger.error('Express error handler', {error:err.message});
    res.status(err.status || 500);
    res.render('error', {
        message: err.message
    });
});

/* Start server */
var options = {
    key: fs.readFileSync(appConf.ssl.path + '/' + appConf.ssl.key),
    cert: fs.readFileSync(appConf.ssl.path + '/' + appConf.ssl.cert)
}

https.globalAgent.options.rejectUnauthorized = false;

https.createServer(options, app).listen(app.get('port'), function() {
    logger.info(pkg.name.toUpperCase(), 'listening on port', app.get('port'), 'with ssl communication');
});

http.createServer(function(req, res) {
    res.writeHead(301, {'Location': 'https://' + req.headers.host + req.url});
    res.end();
}).listen(appConf.config.port);
