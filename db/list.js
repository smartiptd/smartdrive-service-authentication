'use strict';

/* Mongoose model reference name */
define('MODEL_CLIENT', 'Client');
define('MODEL_AUTHORIZATION_CODE', 'AuthorizationCode');
define('MODEL_ACCESS_TOKEN', 'AccessToken');
define('MODEL_REFRESH_TOKEN', 'RefreshToken');
define('MODEL_USER', 'User');
define('MODEL_SUBSYS', 'Subsys');
define('MODEL_CUSTOMER', 'Customer');
define('MODEL_DISTRIBUTE', 'Distribute');
define('MODEL_SUBSCRIBE', 'Subscribe');

/**
* Define constant properties
*
* @param {string} name
* @param {Object} value
*/
function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    });
}
