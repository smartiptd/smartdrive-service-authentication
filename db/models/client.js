'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var modelList = require('../list');

/* Mongoose schema */
var clientSchema = new Schema({
    clientSecret: {type:String, require:true},
    clientSignature: {type:String, require:true},
    clientType: {type:String, require:true},
    subsysType: {type:String, require:true},
    redirectURI: {type:String},
    warrantyBegin: {type:Date},
    warrantyEnd: {type:Date},
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
clientSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.Client = mongoose.model(modelList.MODEL_CLIENT, clientSchema);
