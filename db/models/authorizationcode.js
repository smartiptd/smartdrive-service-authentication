'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var modelList = require('../list');

/* Mongoose schema */
var authorizationCodeSchema = new Schema({
    code: {type:String},
    clientID: {type:Schema.Types.ObjectId, ref:modelList.MODEL_CLIENT},
    userID: {type:Schema.Types.ObjectId, ref:modelList.MODEL_USER},
    redirectURI: {type:String},
    scope: [{type:String}],
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
authorizationCodeSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.AuthorizationCode = mongoose.model(modelList.MODEL_AUTHORIZATION_CODE, authorizationCodeSchema);
