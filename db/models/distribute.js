'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var modelList = require('../list');

/* DB binding schema */
var distributeSchema = new Schema({
    distributeSubsys: {type:Schema.Types.ObjectId, ref:modelList.MODEL_SUBSYS, index:{unique:true}},
    distributeCustomer: {type:Schema.Types.ObjectId, ref:modelList.MODEL_CUSTOMER},
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
distributeSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.Distribute = mongoose.model(modelList.MODEL_DISTRIBUTE, distributeSchema);
