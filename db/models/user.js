'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var common = require('smartdrive-lib').common;
var e_num = require('smartdrive-lib').enum;
var logger = require('smartdrive-lib').logger;
var bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;
var modelList = require('../list');

/* Mongoose schema */
var userSchema = new Schema({
    email: {type:String, require:true, index:{unique:true}},
    password: {type:String, require:true, default:''},
    facebookToken: {type:String, require:true, default:''},
    googleToken: {type:String, require:true, default:''},
    givenName: {type:String, require:true},
    familyName: {type:String, require:true},
    role: {type:String, require:true, default:e_num.oauthUserRole.MEMU},
    resetPasswordToken: {type:String},
    resetPasswordExpire: {type:Date},
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
userSchema.pre('save', function(callback) {
    var user = this;
    if(common.checkValidate(user.password)) {
        if(!user.isModified('password')) return callback();
        bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
            if(err) {
                logger.error('bcrypt > genSalt', {error:err});
                return callback(err);
            }
            bcrypt.hash(user.password, salt, function(err, hash) {
                if(err) {
                    logger.error('bcrypt > hash', {error:err});
                    return callback(err);
                }
                user.password = hash;
                return callback();
            });
        });
    } else {
        callback();
    }
});

userSchema.methods.comparePassword = function(candidatePassword, callback) {
    var user = this;
    bcrypt.compare(candidatePassword, user.password, function(err, isMatch) {
        if(err) {
            logger.error('bcrypt > compare', {error:err});
            return callback(err);
        }
        return callback(null, isMatch);
    });
}

userSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.User = mongoose.model(modelList.MODEL_USER, userSchema);
