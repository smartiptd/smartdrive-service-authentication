'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var modelList = require('../list');

/* Mongoose schema */
var customerSchema = new Schema({
    mobile: {type:String, require:true, index:{unique:true}},
    givenName: {type:String, require:true},
    familyName: {type:String, require:true},
    email: {type:String, require:true, default:''},
    address: {type:String, require:true, default:''},
    note: {type:String, require:true, default:''},
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
customerSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.Customer = mongoose.model(modelList.MODEL_CUSTOMER, customerSchema);
