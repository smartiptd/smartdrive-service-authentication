'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var modelList = require('../list');

/* Mongoose schema */
var subsysSchema = new Schema({
    serial: {type:String, require:true, index:{unique:true}},
    name: {type:String},
    type: {type:String, require:true},
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
subsysSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.Subsys = mongoose.model(modelList.MODEL_SUBSYS, subsysSchema);
