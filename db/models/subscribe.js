'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var modelList = require('../list');

/* DB binding schema */
var subscribeSchema = new Schema({
    subscribeSubsys: {type:Schema.Types.ObjectId, ref:modelList.MODEL_SUBSYS, index:{unique:true}},
    subscribeUser: {type:Schema.Types.ObjectId, ref:modelList.MODEL_USER},
    subscribeClient: {type:Schema.Types.ObjectId, ref:modelList.MODEL_CLIENT},
    subsysUsers: [{type:Schema.Types.ObjectId, ref:modelList.MODEL_USER}],
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
subscribeSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.Subscribe = mongoose.model(modelList.MODEL_SUBSCRIBE, subscribeSchema);
