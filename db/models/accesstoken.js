'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var modelList = require('../list');

/* Mongoose schema */
var accessTokenSchema = new Schema({
    token: {type:String, require:true},
    clientID: {type:Schema.Types.ObjectId, ref:modelList.MODEL_CLIENT},
    userID: {type:Schema.Types.ObjectId, ref:modelList.MODEL_USER},
    expireDate: {type:Date},
    scope: [{type:String}],
    challengeNum: {type:Number},
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
accessTokenSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.AccessToken = mongoose.model(modelList.MODEL_ACCESS_TOKEN, accessTokenSchema);
