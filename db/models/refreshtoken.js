'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var modelList = require('../list');

/* Mongoose schema */
var refreshTokenSchema = new Schema({
    token: {type:String, require:true},
    clientID: {type:Schema.Types.ObjectId, ref:modelList.MODEL_CLIENT},
    userID: {type:Schema.Types.ObjectId, ref:modelList.MODEL_USER},
    scope: [{type:String}],
    createDate: {type:Date, require:true, default:Date.now()},
    updateDate: {type:Date, require:true, default:Date.now()}
});

/* Schema middleware */
refreshTokenSchema.pre('update', function() {
    this.update({},{$set: {updateDate: new Date()}});
});

/* Export model */
exports.RefreshToken = mongoose.model(modelList.MODEL_REFRESH_TOKEN, refreshTokenSchema);
