'use strict';

/* Module dependencies */
var mongoose = require('mongoose');

/*******************
* Export DB Models *
*******************/
module.exports.Client = require('./models/client').Client;
module.exports.AuthorizationCode = require('./models/authorizationcode').AuthorizationCode;
module.exports.AccessToken = require('./models/accesstoken').AccessToken;
module.exports.RefreshToken = require('./models/refreshtoken').RefreshToken;
module.exports.User = require('./models/user').User;
module.exports.Subsys = require('./models/subsys').Subsys;
module.exports.Customer = require('./models/customer').Customer;
module.exports.Distribute = require('./models/distribute').Distribute;
module.exports.Subscribe = require('./models/subscribe').Subscribe;

/**
* Convert String to ObjectId
*
* @param {String} str
*/
module.exports.parseObjectId = function(str) {
    return mongoose.Types.ObjectId(str);
}
