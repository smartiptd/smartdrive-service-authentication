'use strict';

/* Module dependencies */
var async = require('async');
var common = require('smartdrive-lib').common;
var db = require('../db');

/**
* Directory Based API
*
* @param {Object} passport object
* @param {Object} express app object
*/
exports = module.exports = function(passport, app) {

    /* Authentication Service API */
    app.get('/api', function(req, res) {
        res.json({name: 'Authentication Service API'});
    });

    app.get('/api/subsys/info', function(req, res) {
        try {
            if(req.query.client || req.query.user) {
                var query = {};
                if(req.query.client) query.subscribeClient = req.query.client;
                if(req.query.user) query.subscribeUser = req.query.user;
                db.Subscribe.find(query, function(err, _subscribes) {
                    if(err || !_subscribes) {
                        res.status(400);
                        res.json({error:'invalid_subscription'});
                    } else {
                        var subsys = [];
                        async.each(_subscribes, function(_subscribe, cb) {
                            if(_subscribe) {
                                if (subsys.indexOf(_subscribe) < 0) {
                                    db.Subsys.findById(_subscribe.subscribeSubsys, function(err, _subsys) {
                                        if (err) {
                                            res.status(400);
                                            res.json({error:'invalid_subsys'});
                                            cb();
                                        } else {
                                            var info = {
                                                id: _subsys._id,
                                                type: _subsys.type.toLowerCase()
                                            };
                                            subsys.push(info);
                                            cb();
                                        }
                                    });
                                } else {
                                    cb();
                                }
                            } else {
                                cb();
                            }
                        }, function(err) {
                            if(err) {
                                res.status(400);
                                res.json(err);
                            } else {
                                res.json({subsys:subsys});
                            }
                        });
                    }
                });
            } else {
                res.status(400);
                res.json({error:'invalid_audience'});
            }
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });

    app.get('/api/user/info', passport.authenticate('bearer', {session:false}), function(req, res) {
        try {
            res.json({
                userID:req.user._id,
                familyName:req.user.familyName,
                givenName:req.user.givenName,
                email:req.user.email,
                role:req.user.role
            });
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });

    app.get('/api/token/info', passport.authenticate('bearer', {session:false}), function(req, res) {
        try {
            if(req.query.access_token) {
                db.AccessToken.findOne({token:req.query.access_token}, function(err, _token) {
                    if(err || !_token || !_token.expireDate) {
                        res.status(400);
                        res.json({error:'invalid_token'});
                    } else if (new Date() > _token.expireDate) {
                        res.status(400);
                        res.json({error:'invalid_token'});
                    } else {
                        var expirationLeft = Math.floor((_token.expireDate.getTime() - new Date().getTime()) / 1000);
                        if (expirationLeft <= 0) {
                            res.status(400);
                            res.json({error:'invalid_token'});
                        } else {
                            db.Subscribe.find({subscribeClient:_token.clientID, subscribeUser:_token.userID}, function(err, _subscribes) {
                                if(err || !_subscribes) {
                                    res.status(400);
                                    res.json({error:'invalid_subscription'});
                                } else {
                                    var subsys = [];
                                    async.each(_subscribes, function(_subscribe, cb) {
                                        if(_subscribe) {
                                            if (subsys.indexOf(_subscribe) < 0) {
                                                subsys.push(_subscribe.subscribeSubsys);
                                            }
                                        }
                                        cb();
                                    }, function(err) {
                                        if(err) {
                                            res.status(400);
                                            res.json(err);
                                        } else {
                                            if (subsys && subsys.length > 0) {
                                                db.Client.findById(_token.clientID, function(err, _client) {
                                                    if(err || !_client) {
                                                        res.status(400);
                                                        res.json({error:'invalid_token'});
                                                    } else {
                                                        res.json({
                                                            audience:_client._id,
                                                            type:_client.clientType,
                                                            subsys:subsys,
                                                            scope:_token.scope,
                                                            expires_in:expirationLeft
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else {
                res.status(400);
                res.json({error:'invalid_token'});
            }
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });

    app.get('/api/challenge/mqtt/broker', passport.authenticate('bearer', {session:false}), function(req, res) {
        try {
            if (req.query.access_token) {
                if (req.query.challenge_text) {
                    var accessToken = req.query.access_token,
                        challengeText = req.query.challenge_text;
                    db.AccessToken.findOne({token:accessToken}, function(err, _accessToken) {
                        if (err) {
                            res.status(400);
                            res.json(err);
                        } else {
                            if (!_accessToken) {
                                res.status(400);
                                res.json({error:'invalid_token'});
                            } else {
                                db.Client.findById(_accessToken.clientID, function(err, _client) {
                                    if (err) {
                                        res.status(400);
                                        res.json(err);
                                    } else {
                                        if (!_client) {
                                            res.status(400);
                                            res.json({error:'invalid_token'});
                                        } else {
                                            var clientSignature = _client.clientSignature;
                                            var challengeCode = common.encrypt(challengeText, clientSignature);
                                            res.json({challenge_code:challengeCode});
                                        }
                                    }
                                });
                            }
                        }
                    });
                } else {
                    res.status(400);
                    res.json({error:'invalid_challenge_text'});
                }
            } else {
                res.status(400);
                res.json({error:'invalid_token'});
            }
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });

    app.get('/api/challenge/mqtt/client', passport.authenticate('bearer', {session:false}), function(req, res) {
        try {
            if (req.query.access_token) {
                if (req.query.challenge_code) {
                    var accessToken = req.query.access_token,
                        challengeCode = req.query.challenge_code;
                    db.AccessToken.findOne({token:accessToken}, function(err, _accessToken) {
                        if (err) {
                            res.status(400);
                            res.json(err);
                        } else {
                            if (!_accessToken) {
                                res.status(400);
                                res.json({error:'invalid_token'});
                            } else {
                                db.Client.findById(_accessToken.clientID, function(err, _client) {
                                    if (err) {
                                        res.status(400);
                                        res.json(err);
                                    } else {
                                        if (!_client) {
                                            res.status(400);
                                            res.json({error:'invalid_token'});
                                        } else {
                                            var clientSignature = _client.clientSignature;
                                            var challengeNum = _accessToken.challengeNum;
                                            var decryptedCode = common.decrypt(challengeCode, clientSignature);
                                            var decryptedCodeNum = Number(decryptedCode);
                                            if (decryptedCodeNum && typeof decryptedCodeNum == 'number') {
                                                var modResult = decryptedCodeNum % challengeNum;
                                                var challengeResult = (modResult == 0) ? true : false;
                                                res.json({result:challengeResult});
                                            } else {
                                                res.status(400);
                                                res.json({error:'invalid_challenge_code'});
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                } else {
                    res.status(400);
                    res.json({error:'invalid_challenge_code'});
                }
            } else {
                res.status(400);
                res.json({error:'invalid_token'});
            }
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });
}
