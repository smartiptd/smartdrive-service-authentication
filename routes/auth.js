'use strict';

/* Module dependencies */
var config = require('config');
var request = require('request');
var constant = require('smartdrive-lib').constant;
var db = require('../db');
var LocalStrategy = require('passport-local').Strategy,
    BasicStrategy = require('passport-http').BasicStrategy,
    ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy,
    BearerStrategy = require('passport-http-bearer').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    TokenStrategy = require('passport-token').Strategy;

var appConf = config.get('app');

/**
* Authentication process
*
* @param {Object} passport object
*/
module.exports = function(passport) {

    /* Passport session setup */
    passport.serializeUser(function(user, done) {
        try {
            done(null, user);
        } catch(err) {
            done(err);
        }
    });

    passport.deserializeUser(function(user, done) {
        try {
            done(null, user);
        } catch(err) {
            done(err);
        }
    });

    /**************************
    * Passport Local Strategy *
    **************************/
    passport.use('signup', new LocalStrategy({
        usernameField:'email',
        passwordField:'password',
        passReqToCallback: true
    }, function(req, email, password, done) {
        try {
            var confirmPass = req.body.confirmpass,
                givenName = req.body.givenname,
                familyName = req.body.familyname;
            db.User.findOne({email:email}, function(err, _user) {
                if(err) {
                    return done(err);
                } else {
                    if(_user) {
                        if(_user.password !== constant.BLANK) {
                            req.flash('error', 'Someone already has that email!');
                            return done(null, false);
                        } else {
                            _user.password = password;
                            _user.givenName = givenName;
                            _user.familyName = familyName;
                            _user.updateDate = new Date();
                            _user.save(function(err) {
                                if(err) {
                                    return done(null, false);
                                } else {
                                    return done(null, _user);
                                }
                            });
                        }
                    } else {
                        if(confirmPass != password) {
                            req.flash('error', 'These passwords don\'t match. Try again?');
                            return done(null, false);
                        } else {
                            var _newUser = new db.User();
                            _newUser.email = email;
                            _newUser.password = password;
                            _newUser.givenName = givenName;
                            _newUser.familyName = familyName;
                            _newUser.save(function(err) {
                                if(err) {
                                    return done(null, false);
                                } else {
                                    return done(null, _newUser);
                                }
                            });
                        }
                    }
                }
            });
        } catch(err) {
            done(err);
        }
    }));

    passport.use('signin', new LocalStrategy({
        usernameField:'email',
        passwordField:'password',
        passReqToCallback: true
    }, function(req, email, password, done) {
        try {
            db.User.findOne({email:email}, function(err, _user) {
                if(err) {
                    return done(err);
                } else {
                    if(!_user) {
                        req.flash('error', 'Please enter a valid email address!');
                        return done(null, false);
                    }  else {
                        _user.comparePassword(password, function(err, isMatch) {
                            if(err) {
                                return done(err);
                            } else {
                                if(!isMatch) {
                                    req.flash('error', 'The email and password you entered don\'t match. Try again?');
                                    return done(null, false);
                                } else {
                                    return done(null, _user);
                                }
                            }
                        });
                    }
                }
            });
        } catch(err) {
            done(err);
        }
    }));

    /********************************************
    * Basic Strategy & Client Password Strategy *
    ********************************************/
    passport.use(new BasicStrategy(
        function(clientID, clientSecret, done) {
            try {
                db.Client.findById(clientID, function(err, _client) {
                    if(err) {
                        return done(err);
                    } else {
                        if(!_client) {
                            return done(null, false);
                        } else {
                            if(_client.clientSecret != clientSecret) {
                                return done(null, false);
                            } else {
                                return done(null, _client);
                            }
                        }
                    }
                });
            } catch(err) {
                done(err);
            }
        }
    ));

    passport.use(new ClientPasswordStrategy(
        function(clientID, clientSecret, done) {
            try {
                db.Client.findById(clientID, function(err, _client) {
                    if(err) {
                        return done(err);
                    } else {
                        if(!_client) {
                            return done(null, false);
                        } else {
                            if(_client.clientSecret !== clientSecret) {
                                return done(null, false);
                            } else {
                                return done(null, _client);
                            }
                        }
                    }
                });
            } catch(err) {
                done(err);
            }
        }
    ));

    /******************
    * Bearer Strategy *
    ******************/
    passport.use(new BearerStrategy(
        function(accessToken, done) {
            try {
                db.AccessToken.findOne({token:accessToken}, function(err, _accessToken) {
                    if(err) {
                        return done(err);
                    } else {
                        if(!_accessToken) {
                            return done(null, false);
                        } else {
                            if(_accessToken.expireDate < new Date()) {
                                _accessToken.remove(function(err) {
                                    if(err) {
                                        return done(err);
                                    } else {
                                        return done(null, false);
                                    }
                                });
                            } else {
                                db.User.findById(_accessToken.userID, function(err, _user) {
                                    if(err) {
                                        return done(err);
                                    } else {
                                        if(!_user) {
                                            return done(null, false);
                                        } else {
                                            var info = {scope:_accessToken.scope};
                                            return done(null, _user, info);
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            } catch(err) {
                done(err);
            }
        }
    ));

    /**************************
    * Passport Token Strategy *
    **************************/
    passport.use(new TokenStrategy(function(username, token, done) {
        try {
            if (username == 'google') {
                var options = {
                    method: 'GET',
                    url: appConf.auth.token.tokenInfoURL,
                    rejectUnauthorized: appConf.ssl.options.rejectUnauthorized,
                    qs: { id_token: token },
                    headers: { 'cache-control': 'no-cache' }
                }
                request(options, function(err, res, _body) {
                    if (err) {
                        return done(err);
                    } else {
                        var _tokenInfo = JSON.parse(_body);
                        if (_tokenInfo && !_tokenInfo.error_description) {
                            console.log(_tokenInfo);
                            return done(null, true);
                        } else {
                            return done(null, false);
                        }
                    }
                });
            } else if (username == 'facebook') {

            } else {
                return done(null, false);
            }
        } catch(err) {
            done(err);
        }
    }));

    /*****************************
    * Passport Facebook Strategy *
    *****************************/
    var facebookAuthConf = config.get('app.auth.facebook');
    passport.use(new FacebookStrategy({
        clientID:facebookAuthConf.clientID,
        clientSecret:facebookAuthConf.clientSecret,
        callbackURL:facebookAuthConf.callbackURL
    }, function(token, refreshToken, profile, done) {
        try {
            db.User.findOne({email:profile.emails[0].value}, function(err, _user) {
                if(err) {
                    return done(err);
                } else {
                    if(_user) {
                        if(_user.facebookToken !== token) {
                            _user.facebookToken = token;
                            _user.givenName = profile.name.givenName;
                            _user.familyName = profile.name.familyName;
                            _user.updateDate = new Date();
                            _user.save(function(err) {
                                if(err) {
                                    return done(null, false);
                                } else {
                                    return done(null, _user);
                                }
                            });
                        } else {
                            return done(null, _user);
                        }
                    } else {
                        var _newUser = new db.User();
                        _newUser.email = profile.emails[0].value;
                        _newUser.facebookToken = token;
                        _newUser.givenName = profile.name.givenName;
                        _newUser.familyName = profile.name.familyName;
                        _newUser.save(function(err) {
                            if(err) {
                                return done(null, false);
                            } else {
                                return done(null, _newUser);
                            }
                        });
                    }
                }
            });
        } catch(err) {
            done(err);
        }
    }));

    /***************************
    * Passport Google Strategy *
    ***************************/
    var googleAuthConf = config.get('app.auth.google');
    passport.use(new GoogleStrategy({
        clientID:googleAuthConf.clientID,
        clientSecret:googleAuthConf.clientSecret,
        callbackURL:googleAuthConf.callbackURL
    }, function(token, refreshToken, profile, done) {
        try {
            db.User.findOne({email:profile.emails[0].value}, function(err, _user) {
                if(err) {
                    return done(err);
                } else {
                    if(_user) {
                        if(_user.googleToken !== token) {
                            _user.googleToken = token;
                            _user.givenName = profile.name.givenName;
                            _user.familyName = profile.name.familyName;
                            _user.updateDate = new Date();
                            _user.save(function(err) {
                                if(err) {
                                    return done(null, false);
                                } else {
                                    return done(null, _user);
                                }
                            });
                        } else {
                            return done(null, _user);
                        }
                    } else {
                        var _newUser = new db.User();
                        _newUser.email = profile.emails[0].value;
                        _newUser.googleToken = token;
                        _newUser.givenName = profile.name.givenName;
                        _newUser.familyName = profile.name.familyName;
                        _newUser.save(function(err) {
                            if(err) {
                                return done(null, false);
                            } else {
                                return done(null, _newUser);
                            }
                        });
                    }
                }
            });
        } catch(err) {
            done(err);
        }
    }));
}
