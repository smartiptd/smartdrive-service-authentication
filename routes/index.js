'use strict';

/* Module dependencies */
var oauth = require('./oauth');
var login = require('connect-ensure-login');

/* Reference URL path */
var URL_INDEX = '/',
    URL_OAUTH_AUTHORIZE = '/oauth/authorize',
    URL_OAUTH_DECISION = '/oauth/decision',
    URL_OAUTH_TOKEN = '/oauth/token',
    URL_AUTH_TOKEN = '/auth/token',
    URL_AUTH_FACEBOOK = '/auth/facebook',
    URL_AUTH_FACEBOOK_CALLBACK = '/auth/facebook/callback',
    URL_AUTH_GOOGLE = '/auth/google',
    URL_AUTH_GOOGLE_CALLBACK = '/auth/google/callback',
    URL_SIGNUP = '/signup',
    URL_SIGNIN = '/signin',
    URL_FORGOT = '/forgot',
    URL_RESET = '/reset/:token',
    URL_HOME = '/home',
    URL_ADMIN = '/admin',
    URL_USER = '/user',
    URL_GATEWAY = '/gateway',
    URL_AAF = '/aaf',
    URL_APIDOC = '/apidoc',
    URL_SIGNOUT = '/signout',
    URL_ALL = '*';

/* Reference web page */
var PAGE_SIGNIN = 'signin',
    PAGE_HOME = 'home',
    PAGE_ADMIN = 'admin',
    PAGE_USER = 'user',
    PAGE_GATEWAY = 'gateway',
    PAGE_AAF = 'aaf',
    PAGE_APIDOC = 'apidoc';

/**
* Routing process
*
* @param {Object} passport object
* @param {Object} express app objectc
*/
module.exports = function(passport, app) {

    /* Authentication process route */
    require('./auth')(passport);

    /* API route */
    require('./api')(passport, app);

    /* Index route */
    app.get(URL_INDEX, function(req, res) {
        if (req.isAuthenticated()) {
            res.render(PAGE_HOME, {user:req.user, message:req.flash()});
        } else {
            res.redirect(URL_SIGNIN);
        }
    });

    /* Local oauth authentication route */
    app.get(URL_OAUTH_AUTHORIZE, login.ensureLoggedIn(), oauth.authorization, function(req, res) {
        res.render(PAGE_AUTHORIZE, {
            transactionID:req.oauth2.transactionID,
            user:req.user,
            client:req.oauth2.client
        });
    });

    app.post(URL_OAUTH_DECISION, login.ensureLoggedIn(), oauth.decision);

    app.post(URL_OAUTH_TOKEN, passport.authenticate(['basic', 'oauth2-client-password'], {session:false}), oauth.token);

    /* Token authentication route */
    app.put(URL_AUTH_TOKEN, passport.authenticate('token'), function(req, res) {

    });

    /* Facebook authentication route */
    app.get(URL_OAUTH_FACEBOOK, passport.authenticate('facebook', {
        scope: ['email', 'public_profile']
    }));

    app.get(URL_OAUTH_FACEBOOK_CALLBACK, passport.authenticate('facebook', {
        successRedirect:URL_INDEX,
        failureRedirect:URL_SIGNIN
    }));

    /* Google authentication route */
    app.get(URL_OAUTH_GOOGLE, passport.authenticate('google', {
        scope: ['email', 'profile']
    }));

    app.get(URL_OAUTH_GOOGLE_CALLBACK, passport.authenticate('google', {
        successRedirect:URL_INDEX,
        failureRedirect:URL_SIGNIN
    }));

    /* Web authentication route */
    app.get(URL_SIGNUP, function(req, res) {
        res.render(PAGE_SIGNUP, {message:req.flash()});
    });

    app.get(URL_SIGNIN, function(req, res) {
        res.render(PAGE_SIGNIN, {message:req.flash()});
    });

    app.get(URL_FORGOT, function(req, res) {
        res.render(PAGE_FORGOT, {message:req.flash()});
    });

    app.get(URL_RESET, function(req, res) {
        redeem.getReset(req, res, PAGE_RESET, URL_FORGOT);
    });

    app.post(URL_SIGNUP, passport.authenticate('signup', {
        successRedirect:URL_INDEX,
        failureRedirect:URL_SIGNUP,
        successFlash:true,
        failureFlash:true
    }));

    app.post(URL_SIGNIN, passport.authenticate('signin', {
        successReturnToOrRedirect:URL_INDEX,
        failureRedirect:URL_SIGNIN,
        successFlash:true,
        failureFlash:true
    }));

    app.post(URL_FORGOT, function(req, res, next) {
        redeem.postForgot(req, res, URL_FORGOT, URL_FORGOT);
    });

    app.post(URL_RESET, function(req, res, next) {
        redeem.postReset(req, res, URL_INDEX, URL_FORGOT, req.originalUrl);
    });

    /* Management website route */
    app.get(URL_HOME, login.ensureLoggedIn(), function(req, res) {
        res.render(PAGE_HOME, {user:req.user, message:req.flash()});
    });

    app.get(URL_ADMIN, login.ensureLoggedIn(), function(req, res) {
        res.render(PAGE_ADMIN, {user:req.user, message:req.flash()});
    });

    app.get(URL_USER, login.ensureLoggedIn(), function(req, res) {
        res.render(PAGE_USER, {user:req.user, message:req.flash()});
    });

    app.get(URL_GATEWAY, login.ensureLoggedIn(), function(req, res) {
        res.render(PAGE_GATEWAY, {user:req.user, message:req.flash()});
    });

    app.get(URL_AAF, login.ensureLoggedIn(), function(req, res) {
        res.render(PAGE_AAF, {user:req.user, message:req.flash()});
    });

    app.get(URL_APIDOC, login.ensureLoggedIn(), function(req, res) {
        res.render(PAGE_APIDOC, {user:req.user, message:req.flash()});
    });

    app.get(URL_SIGNOUT, function(req, res) {
        req.logout();
        res.redirect(URL_INDEX);
    });

    app.all(URL_ALL, function(req, res) {
        res.redirect(URL_INDEX);
    });
}
