'use strict';

/* Module dependencies */
var config = require('config');
var oauth2orize = require('oauth2orize');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var common = require('smartdrive-lib').common;
var db = require('../db');

var appConf = config.get('app');

/* Create oauth 2.0 server */
var server = oauth2orize.createServer();

/* Register serialialization and deserialization functions */
server.serializeClient(function(client, done) {
	try {
		done(null, client);
	} catch(err) {
		return done(err);
	}
});

server.deserializeClient(function(client, done) {
	try {
		done(null, client);
	} catch(err) {
		return done(err);
	}
});

/*********************************
* Register supported grant types *
*********************************/

/* Grant authorization codes */
server.grant(oauth2orize.grant.code({scopeSeparator: ';'}, function(client, redirectURI, user, res, done) {
	try {
		var now = new Date().getTime(),
			code = crypto.createHmac('sha1', 'authorize_code')
					.update([client._id, now].join())
					.digest('hex');
		var _authCode = new db.AuthorizationCode();
		_authCode.code = code;
		_authCode.clientID = client._id;
		_authCode.userID = user._id;
		_authCode.redirectURI = redirectURI;
		_authCode.scope = res.scope;
		_authCode.save(function(err) {
			if(err) {
				return done(err);
			} else {
				return done(null, code);
			}
		});
	} catch(err) {
		return done(err);
	}
}));

/* Grant implicit authorization */
var tokenLifeTime = (appConf.auth.oauth.tokenLifeTime) ? appConf.auth.oauth.tokenLifeTime : 3600;
server.grant(oauth2orize.grant.token({scopeSeparator: ';'}, function(client, user, res, done) {
	try {
		grantToken(client, user, res.scope, function(err, accessToken, refreshToken, expires_in) {
			if(err) {
				return done(err);
			} else {
				return done(err, accessToken, refreshToken, expires_in);
			}
		});
	} catch(err) {
		return done(err);
	}
}));

/* Exchange autorization codes for access tokens */
server.exchange(oauth2orize.exchange.code(function(client, code, redirectURI, done) {
	try {
		db.AuthorizationCode.findOne({code:code}, function(err, _code) {
			if(err) {
				return done(err);
			} else {
				if(!_code) {
					return done(null, false);
				} else {
					if(client._id.toString() !== _code.clientID.toString()) {
						return done(null, false);
					} else {
						if(redirectURI !== _code.redirectURI) {
							return done(null, false);
						} else {
							grantToken(client, {_id:_code.userID}, _code.scope, function(err, accessToken, refreshToken, expires_in) {
								if(err) {
									return done(err);
								} else {
									return done(err, accessToken, refreshToken, expires_in);
								}
							});
						}
					}
				}
			}
		});
	} catch(err) {
		return done(err);
	}
}));

/* Exchange user id and password for access tokens */
server.exchange(oauth2orize.exchange.password({scopeSeparator: ';'}, function(client, username, password, scope, done) {
	try {
		db.User.findOne({email:username}, function(err, _user) {
			if(err) {
				return done(err);
			} else {
				if(!_user) {
					return done(null, false);
				} else {
					_user.comparePassword(password, function(err, isMatch) {
						if(err) {
							return done(err);
						} else {
							if(!isMatch) {
								return done(null, false);
							} else {
								grantToken(client, {_id:_user._id}, scope, function(err, accessToken, refreshToken, expires_in) {
									if(err) {
										return done(err);
									} else {
										return done(err, accessToken, refreshToken, expires_in);
									}
								});
							}
						}
					});
				}
			}
		});
	} catch(err) {
		return done(err);
	}
}));

/* Exchange the client id and password/secret for an access token */
server.exchange(oauth2orize.exchange.clientCredentials({scopeSeparator: ';'}, function(client, scope, done) {
	try {
		db.Client.findById(client._id, function(err, _client) {
			if(err) {
				return done(err);
			} else {
				if(!_client) {
					return done(null, false);
				} else {
					if(_client.clientSecret !== client.clientSecret) {
						return done(null, false);
					} else {
						db.Subscribe.findOne({subscribeClient:_client._id}, function(err, _subscribe) {
							if(err) {
								return done(err);
							} else {
								if(!_subscribe) {
									return done(null, false);
								} else {
									grantToken(_client, {_id:_subscribe.subscribeUser}, scope, function(err, accessToken, refreshToken, expires_in) {
										if(err) {
											return done(err);
										} else {
											return done(err, accessToken, refreshToken, expires_in);
										}
									});
								}
							}
						});
					}
				}
			}
		});
	} catch(err) {
		return done(err);
	}
}));

/* Exchange the refresh token for an access token */
server.exchange(oauth2orize.exchange.refreshToken({scopeSeparator: ';'}, function(client, refreshToken, scope, done) {
	try {
		db.RefreshToken.findOne({token:refreshToken}, function(err, _refreshToken) {
			if (err) {
				return done(err);
			} else {
				if (!_refreshToken) {
					return done(null, false);
				} else {
					grantToken(client, {_id:_refreshToken.userID}, _refreshToken.scope, function(err, accessToken, refreshToken, expires_in) {
						if(err) {
							return done(err);
						} else {
							db.RefreshToken.remove({token:_refreshToken}).exec();
							return done(err, accessToken, refreshToken, expires_in);
						}
					});
				}
			}
		});
	} catch(err) {
		return done(err);
	}
}));

/*****************
* User Endpoints *
*****************/

/* Authorization endpoint */
exports.authorization = [
	server.authorization(function(clientID, redirectURI, scope, done) {
		try {
			var _id = db.parseObjectId(clientID);
			db.Client.findById(_id, function(err, _client) {
				if(err) {
					return done(err);
				} else {
					if(!_client) {
						return done(null, false);
					} else {
						if(_client.redirectURI !== redirectURI) {
							return done(null, false);
						} else {
							return done(null, _client, redirectURI)
						}
					}
				}
			});
		} catch(err) {
			return done(err);
		}
	})
]

/* Decision endpoint */
exports.decision = [
	server.decision(function(req, done) {
		try {
			return done(null, {scope: req.oauth2.req.scope});
		} catch(err) {
			return done(err);
		}
	})
]

/* Token endpoint */
exports.token = [
	server.token(),
	server.errorHandler()
]

/*******************
* Backend function *
*******************/
var interval = (appConf.auth.oauth.checkExpiredTokens) ? appConf.auth.oauth.checkExpiredTokens : 3600;
setInterval(function () {
	try {
		db.AccessToken.remove({'expireDate':{'$lt':new Date()}}).exec();
	} catch(err) {
		return;
	}
}, interval * 1000);

function grantToken(client, user, scope, done) {
	try {
		if (scope && Array.isArray(scope)) {
			var randNum = common.randomInt(1000, 9999);
			var payload = { challengeNum:randNum };
			var token = jwt.sign(payload, client.clientSecret);
			var splitedToken = token.split('.');
			if(splitedToken && splitedToken.length == 3) {
				var accessToken = splitedToken[1];
				var refreshToken = splitedToken[2];
				/* Store access token */
				var _accessToken = new db.AccessToken();
				_accessToken.token = accessToken;
				_accessToken.clientID = (client) ? client._id : null;
				_accessToken.userID = (user) ? user._id : null;
				_accessToken.scope = scope.filter(function(el) {return el.length != 0});
				_accessToken.expireDate = new Date(new Date().getTime() + (tokenLifeTime * 1000));
				_accessToken.challengeNum = randNum;
				_accessToken.save(function(err) {
					if(err) {
						return done(err);
					} else {
						/* Store refresh token */
						var _refreshToken = new db.RefreshToken();
						_refreshToken.token = refreshToken;
						_refreshToken.clientID = (client) ? client._id : null;
						_refreshToken.userID = (user) ? user._id : null;
						_refreshToken.scope = scope.filter(function(el) {return el.length != 0});
						_refreshToken.save(function(err) {
							if(err) {
								return done(err);
							} else {
								return done(null, accessToken, refreshToken, {expires_in:tokenLifeTime});
							}
						});
					}
				});
			} else {
				return done(null, false);
			}
		} else {
			return done(null, false);
		}
	} catch(err) {
		return done(err);
	}
}
