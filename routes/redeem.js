'use strict';

/* Module dependencies */
var async = require('async');
var crypto = require('crypto');
var moment = require('moment');
var mailer = require('nodemailer');
var config = require('config');
var constant = require('smartdrive-lib').constant,
    common = require('smartdrive-lib').common;
var db = require('../db');

/* Constructor */
var Redeem = function() {}

/**
* Forgot process
*/
Redeem.prototype.postForgot = function(req, res, successRedirect, failureRedirect) {
    async.waterfall([
        function(done) {
            crypto.randomBytes(20, function(err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function(token, done) {
            db.User.findOne({email: req.body.email}, function(err, user) {
                if(!user) {
                    req.flash('error', 'No account with that email address exists.');
                    return res.redirect(failureRedirect);
                } else {
                    user.resetPasswordToken = token;
                    user.resetPasswordExpire = moment().add({hours:1});
                    user.save(function(err) {
                        done(err, token, user);
                    });
                }
            });
        },
        function(token, user, done) {
            var mailTransport = mailer.createTransport('SMTP', config.mail.transport);
            var options = {
                to: user.email,
                from: config.mail.sender,
                subject: 'Demo Pasword Reset',
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                'http://' + req.headers.host + '/reset/' + token + '\n\n' +
                'If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };
            mailTransport.sendMail(options, function(err) {
                if(err) {
                    req.flash('error', 'Cannot send email to ' + user.email + '.');
                } else {
                    req.flash('info', 'An email has been sent to ' + user.email + ' with further instructions.');
                }
                done(err);
            });
        }
    ], function(err) {
        if(err) {
            return res.redirect(failureRedirect);
        } else {
            return res.redirect(successRedirect);
        }
    });
}

/**
* Get reset process
*/
Redeem.prototype.getReset = function(req, res, successRenderPage, failureRedirect) {
    db.User.findOne({resetPasswordToken:req.params.token, resetPasswordExpire:{$gt:Date.now()}}, function(err, user) {
        if(!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect(failureRedirect);
        } else {
            return res.render(successRenderPage, {user:req.user, message:req.flash()});
        }
    });
}

/**
* Post reset process
*/
Redeem.prototype.postReset = function(req, res, successRedirect, forgotFailureRedirect, resetFailureRedirect) {
    async.waterfall([
        function(done) {
            db.User.findOne({resetPasswordToken:req.params.token, resetPasswordExpire:{$gt:Date.now()}}, function(err, user) {
                if(!user) {
                    req.flash('error', 'Password reset token is invalid or has expired.');
                    return res.redirect(forgotFailureRedirect);
                } else {
                    if(req.body.password === constant.BLANK || req.body.confirmpass === constant.BLANK) {
                        req.flash('warning', 'The password cannot be empty!');
                        return res.redirect(resetFailureRedirect);
                    } else if (req.body.password !== req.body.confirmpass) {
                        req.flash('error', 'These passwords don\'t match. Try again?');
                        return res.redirect(resetFailureRedirect);
                    } else {
                        user.password = req.body.password;
                        user.resetPasswordToken = undefined;
                        user.resetPasswordToken = undefined;
                        user.updateDate = new Date();
                        user.updateUser = user.email + '-reset';
                        user.save(function(err, user) {
                            if(err) {
                                return res.redirect(resetFailureRedirect);
                            } else {
                                req.flash('success', 'Success! Your password has been changed');
                                done(err, user);
                            }
                        });
                    }
                }
            });
        },
        function(user, done) {
            var mailTransport = mailer.createTransport('SMTP', config.mail.transport);
            var options = {
                to: user.email,
                from: config.mail.sender,
                subject: 'Your password has been changed',
                text: 'Hello,\n\n' +
                'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
            };
            mailTransport.sendMail(options, function(err) {
                if(err) {
                    req.flash('error', 'Cannot send email to ' + user.email + '.');
                }
                done(err);
            });
        }
    ], function(err) {
        if(err) {
            return res.redirect(resetFailureRedirect);
        } else {
            return res.redirect(successRedirect);
        }
    });
}

exports = module.exports = new Redeem();
