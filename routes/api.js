'use strict';

/* Module dependencies */
var async = require('async');
var login = require('connect-ensure-login');
var redeem = require('./redeem');
var oauth = require('./oauth');
var db = require('../db');

/* Reference API URL path */
var API_ROOT = '/api',
    API_ACCOUNT_REGISTER = '/api/account/register',
    API_INFO_SUBSYS = '/api/info/subsys',
    API_INFO_USER = '/api/info/user',
    API_INFO_TOKEN = '/api/info/token',
    API_MQTT_CHALLENGE = '/api/mqtt/challenge';

/**
* Directory Based API
*
* @param {Object} passport object
* @param {Object} express app object
*/
exports = module.exports = function(passport, app) {

    /***********************/
    /* Directory Based API */
    /***********************/
    app.get(API_ROOT, function(req, res) {
        res.json({name: 'Directory Based API'});
    });

    /* Account API */
    app.post(API_ACCOUNT_REGISTER, function(req, res, done) {
        passport.authenticate('signup', function(err, user) {
            // Generate a 500 error
            if (err) {
                return done(err);
            }
            // Generate a JSON response status
            if (user) {
                return res.send({
                    api:API_ACCOUNT_REGISTER,
                    success:true,
                    message:'Registration completed'
                });
            } else {
                return res.send({
                    api:API_ACCOUNT_REGISTER,
                    success:false,
                    message:req.flash().error[0]
                });
            }
        })(req, res, done);
    });

    /* Info API */
    app.get(API_INFO_SUBSYS, function(req, res) {
        try {
            if(req.query.client || req.query.user) {
                var query = {};
                if(req.query.client) query.subscribeClient = req.query.client;
                if(req.query.user) query.subscribeUser = req.query.user;
                db.Subscribe.find(query, function(err, _subscribes) {
                    if(err || !_subscribes) {
                        res.status(400);
                        res.json({error:'invalid_subscription'});
                    } else {
                        var subsys = [];
                        async.each(_subscribes, function(_subscribe, cb) {
                            if(_subscribe) {
                                if (subsys.indexOf(_subscribe) < 0) {
                                    db.Subsys.findById(_subscribe.subscribeSubsys, function(err, _subsys) {
                                        if (err) {
                                            res.status(400);
                                            res.json({error:'invalid_subsys'});
                                            cb();
                                        } else {
                                            var info = {
                                                id: _subsys._id,
                                                type: _subsys.type.toLowerCase()
                                            };
                                            subsys.push(info);
                                            cb();
                                        }
                                    });
                                } else {
                                    cb();
                                }
                            } else {
                                cb();
                            }
                        }, function(err) {
                            if(err) {
                                res.status(400);
                                res.json(err);
                            } else {
                                res.json({subsys:subsys});
                            }
                        });
                    }
                });
            } else {
                res.status(400);
                res.json({error:'invalid_audience'});
            }
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });

    app.get(API_INFO_USER, passport.authenticate('bearer', {session:false}), function(req, res) {
        try {
            res.json({
                userID:req.user._id,
                familyName:req.user.familyName,
                givenName:req.user.givenName,
                email:req.user.email,
                role:req.user.role
            });
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });

    app.get(API_INFO_TOKEN, passport.authenticate('bearer', {session:false}), function(req, res) {
        try {
            if(req.query.access_token) {
                db.AccessToken.findOne({token:req.query.access_token}, function(err, _token) {
                    if(err || !_token || !_token.expireDate) {
                        res.status(400);
                        res.json({error:'invalid_token'});
                    } else if (new Date() > _token.expireDate) {
                        res.status(400);
                        res.json({error:'invalid_token'});
                    } else {
                        var expirationLeft = Math.floor((_token.expireDate.getTime() - new Date().getTime()) / 1000);
                        if (expirationLeft <= 0) {
                            res.status(400);
                            res.json({error:'invalid_token'});
                        } else {
                            db.Subscribe.find({subscribeClient:_token.clientID, subscribeUser:_token.userID}, function(err, _subscribes) {
                                if(err || !_subscribes) {
                                    res.status(400);
                                    res.json({error:'invalid_subscription'});
                                } else {
                                    var subsys = [];
                                    async.each(_subscribes, function(_subscribe, cb) {
                                        if(_subscribe) {
                                            if (subsys.indexOf(_subscribe) < 0) {
                                                subsys.push(_subscribe.subscribeSubsys);
                                            }
                                        }
                                        cb();
                                    }, function(err) {
                                        if(err) {
                                            res.status(400);
                                            res.json(err);
                                        } else {
                                            if (subsys && subsys.length > 0) {
                                                db.Client.findById(_token.clientID, function(err, _client) {
                                                    if(err || !_client) {
                                                        res.status(400);
                                                        res.json({error:'invalid_token'});
                                                    } else {
                                                        res.json({
                                                            audience:_client._id,
                                                            type:_client.clientType,
                                                            subsys:subsys,
                                                            scope:_token.scope,
                                                            expires_in:expirationLeft
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else {
                res.status(400);
                res.json({error:'invalid_token'});
            }
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });

    /* MQTT Service API */
    app.get(API_MQTT_CHALLENGE, passport.authenticate('bearer', {session:false}), function(req, res) {
        try {
            if (req.query.access_token) {
                if (req.query.challenge_code) {
                    var accessToken = req.query.access_token,
                        challengeCode = req.query.challenge_code;
                    db.AccessToken.findOne({token:accessToken}, function(err, _accessToken) {
                        if (err) {
                            res.status(400);
                            res.json(err);
                        } else {
                            if (!_accessToken) {
                                res.status(400);
                                res.json({error:'invalid_token'});
                            } else {
                                db.Client.findById(_accessToken.clientID, function(err, _client) {
                                    if (err) {
                                        res.status(400);
                                        res.json(err);
                                    } else {
                                        if (!_client) {
                                            res.status(400);
                                            res.json({error:'invalid_token'});
                                        } else {
                                            var challengeNum = _accessToken.challengeNum;
                                            if (challengeCode && typeof challengeCode == 'number') {
                                                var modResult = challengeCode % challengeNum;
                                                var divideResult = challengeCode / challengeNum;
                                                var challengeResult = (modResult == 0) ? true : false;
                                                res.json({
                                                    result:challengeResult,
                                                    value: divideResult
                                                });
                                            } else {
                                                res.status(400);
                                                res.json({error:'invalid_challenge_code'});
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                } else {
                    res.status(400);
                    res.json({error:'invalid_challenge_code'});
                }
            } else {
                res.status(400);
                res.json({error:'invalid_token'});
            }
        } catch (err) {
            res.status(400);
            res.json(err);
        }
    });
}
