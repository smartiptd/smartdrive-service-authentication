#!/bin/sh

# Shell script for preparing for create server certificate

# Constant
PART_CERT=.

FILE_SRV_KEY=$PART_CERT/smartdrive-service-auth.key
FILE_SRV_CSR=$PART_CERT/smartdrive-service-auth.csr
FILE_SRV_CERT=$PART_CERT/smartdrive-service-auth.crt

############################################
# Server key & Certificate signing request #
############################################
echo "Create server key & certificate signing request"

if [ -f $FILE_SRV_KEY ]
    then
    echo "  * exist server key"
else
    echo "  * create server key"
    openssl genrsa -out $FILE_SRV_KEY 2048
    # openssl genrsa -des3 -out $FILE_SRV_KEY 2048
    # openssl ecparam -name sect571r1 -genkey -out $FILE_SRV_KEY
    sleep 1
fi

if [ -f $FILE_SRV_CERT ]
    then
    echo "  * exist server certificate"
else

    if [ -f $FILE_SRV_CSR ]
        then
        echo "  * exist certificate signing request"
    else
        echo "  * create certificate signing request"
        openssl req -sha256 -new -key $FILE_SRV_KEY -out $FILE_SRV_CSR
    fi
fi
